<?php


namespace Pondit\Calculator\AreaCalculator;


class Square
{
   public $number1 ;
   public $number2;

   public function __construct($number1,$number2)
   {
       $this->number1= $number1 ;
       $this->number2= $number2 ;
   }

   public function square(){

       $area =  $this->number1 ** $this->number2;
       return $area;
   }
}