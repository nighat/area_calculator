<?php


namespace Pondit\Calculator\AreaCalculator;


class Triangle
{
    public $base;
    public $height;

    public function __construct($base,$height)
    {
        $this->base = $base;
        $this->height =$height;

    }

    public function triangleArea(){

        $area = $this->base *$this->height/2;
        return $area;

    }
}