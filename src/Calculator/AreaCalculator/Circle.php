<?php


namespace Pondit\Calculator\AreaCalculator;


class Circle
{
   public $radius;
   public $pi;

   public function __construct($radius,$pi)
   {
       $this->radius=$radius;
       $this->pi = $pi;
   }

   public function getArea(){

       $area = $this->pi* $this->radius * $this->radius;
       return $area;
   }
}